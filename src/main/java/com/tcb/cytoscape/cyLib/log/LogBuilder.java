package com.tcb.cytoscape.cyLib.log;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class LogBuilder implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<String> data;

	public LogBuilder(){
		clear();
	}
	
	public void writeDate(){
		Date now = new Date();
		String date = DateFormat.getDateTimeInstance(
	            DateFormat.LONG, DateFormat.LONG).format(now);
		data.add("Time: " + date);
	}
		
	public void writeEmptyLine(){
		data.add("");
	}
	
	public void writeDashedLine(){
		data.add("------------------------");
	}
	
	public void clear(){
		data = new ArrayList<>();
	}
	
	public void write(String s){
		data.add(s);
	}
	
	public void write(LogBuilder log){
		data.addAll(log.data);
	}
	
	public void writeParagraph(String s){
		write(s);
		writeEmptyLine();
	}
		
	public String get(){
		String separator = System.getProperty("line.separator");
		return data.stream()
				.collect(Collectors.joining(separator));
	}
	
	
	
	
}
