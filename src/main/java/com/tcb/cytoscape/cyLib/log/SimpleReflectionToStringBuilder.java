package com.tcb.cytoscape.cyLib.log;

import java.lang.reflect.Field;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class SimpleReflectionToStringBuilder extends ReflectionToStringBuilder {
	public SimpleReflectionToStringBuilder(Object object, ToStringStyle style){
		super(object,style);
	}
	
	@Override
	protected boolean accept(Field field){
		if(isIgnored(field)) return false;
		else return super.accept(field);
	}
	
	private boolean isIgnored(Field field){
			boolean result = field.getAnnotation(NotReported.class) != null;
			return result;
	}
}
