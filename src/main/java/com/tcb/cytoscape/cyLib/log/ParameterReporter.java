package com.tcb.cytoscape.cyLib.log;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;


public interface ParameterReporter {
	
	public default String reportParameters() {
		return getToStringBuilder().build();
	}
	
	public default ReflectionToStringBuilder getToStringBuilder(){
		SimpleRecursiveToStringStyle style = new SimpleRecursiveToStringStyle();
		return new SimpleReflectionToStringBuilder(this,style);
	}
}
