package com.tcb.cytoscape.cyLib.log;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.builder.MultilineRecursiveToStringStyle;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyIdentifiable;
import org.cytoscape.model.CyNode;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;

public class SimpleRecursiveToStringStyle extends MultilineRecursiveToStringStyle {
	
	private static final String autoValuePrefix = "^AutoValue_";
	
	public SimpleRecursiveToStringStyle(){

		//this.setUseClassName(false);
		this.setUseShortClassName(true);
		this.setUseIdentityHashCode(false);
	}
	
	@Override
	protected boolean accept(Class<?> clazz){
		if(clazz.isEnum()) return false;
		else if(CyIdentifiable.class.isAssignableFrom(clazz)) return false;
		else if(ParameterReporter.class.isAssignableFrom(clazz)) return false;
		else return true;
	}
		
	@Override
	public void appendDetail(StringBuffer buffer, String fieldName, Object o){
		if(o instanceof Path) buffer.append(((Path)o).toString());
		else if(o instanceof Optional) buffer.append(((Optional)o).orElse(null));
		else if(o instanceof ParameterReporter) buffer.append(((ParameterReporter)o).reportParameters());
		else super.appendDetail(buffer, fieldName, o);
	}
	
	@Override
	protected void appendClassName(StringBuffer buffer, Object o){
		if(o instanceof Collection) return;
		String shortClassName = o.getClass().getSimpleName();
		shortClassName = shortClassName.replaceFirst(autoValuePrefix, "");
		buffer.append(shortClassName);
	}
		
	
	
	
		
}
