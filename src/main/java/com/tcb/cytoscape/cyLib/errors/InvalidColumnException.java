package com.tcb.cytoscape.cyLib.errors;

public class InvalidColumnException extends IllegalArgumentException {
	public InvalidColumnException(String message){
		super(message);
	}
}
