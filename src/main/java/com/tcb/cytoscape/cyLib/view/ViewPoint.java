package com.tcb.cytoscape.cyLib.view;

import java.io.Serializable;

public class ViewPoint implements Serializable {
	Double centerX;
	Double centerY;
	Double scaling;
	
	public ViewPoint(Double x, Double y, Double scaling){
		this.centerX = x;
		this.centerY = y;
		this.scaling = scaling;
	}
	
	public Double getX(){
		return centerX;
	}
	
	public Double getY(){
		return centerY;
	}
	
	public Double getScaling(){
		return scaling;
	}
	
	public void printScaleFactor(){
		System.out.println(String.format(String.format("Scale factor: %.2f", scaling)));
	}
	
}
