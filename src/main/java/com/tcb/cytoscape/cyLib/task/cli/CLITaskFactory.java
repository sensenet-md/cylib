package com.tcb.cytoscape.cyLib.task.cli;

import java.util.Properties;

import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;

public interface CLITaskFactory {
	public TaskIterator createTaskIterator();
	public String getCommandName();
	public String getCommandDescription();
	public Properties getProperties();
}
