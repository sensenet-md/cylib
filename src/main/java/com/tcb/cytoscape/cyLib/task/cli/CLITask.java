package com.tcb.cytoscape.cyLib.task.cli;

import org.cytoscape.work.TaskIterator;

public interface CLITask {
	public TaskIterator createWrappedTasks();
}
