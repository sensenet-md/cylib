package com.tcb.cytoscape.cyLib.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class SplitFileOutputStream extends OutputStream {

	private FileFactory fileFactory;
	private List<File> files;
	private FileOutputStream fStream;
	private int maxFileBytes;
	
	private static final int defaultMaxFileBytes = Integer.MAX_VALUE;

	public SplitFileOutputStream(FileFactory fileFactory, int maxFileBytes) throws IOException {
		this.files = new ArrayList<>();
		this.fileFactory = fileFactory;
		this.maxFileBytes = maxFileBytes;
		setNewFileStream();
		verifyState();
	}
	
	public SplitFileOutputStream(FileFactory fileFactory) throws IOException {
		this(fileFactory, defaultMaxFileBytes);
	}
	
	private void verifyState(){
		if(maxFileBytes < 1) throw new IllegalArgumentException("Max byte number must be >= 1");
	}
			
	@Override
	public void write(int b) throws IOException {
		FileChannel fc = fStream.getChannel();
		if(fc.size() >= maxFileBytes) setNewFileStream();
		fStream.write(b);
	}
	
	@Override
	public void write(byte b[], int off, int len) throws IOException {
		if(len > maxFileBytes) {
			String msg = String.format("Cannot fit array of length %d in one file", len);
			throw new IOException(msg);
		}
		FileChannel fc = fStream.getChannel();
		if(len > maxFileBytes - fc.size()) {
			setNewFileStream();
		}
		fStream.write(b,off,len);
	}
	
	@Override
	public void write(byte b[]) throws IOException {
		write(b,0,b.length);
	}
	
	@Override
	public void close() throws IOException {
		fStream.close();
	}
	
	@Override
	public void finalize() {
		try{fStream.close();}
		catch(Exception e){System.out.println("Could not close file stream");};
	}
	
	private synchronized void setNewFileStream() throws IOException {
		File f = fileFactory.create();
		if(fStream != null) {
			fStream.flush();
			fStream.close();
		}
		this.fStream = new FileOutputStream(f);
		files.add(f);
	}
	
	public List<File> getFiles(){
		return files;
	}

}
