package com.tcb.cytoscape.cyLib.io;

import java.io.File;
import java.io.IOException;

public interface FileFactory {
	public File create() throws IOException;
}
