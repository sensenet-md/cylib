package com.tcb.cytoscape.cyLib.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SplitFileInputStream extends InputStream {

	private InputStream iStream;

	public SplitFileInputStream(List<File> files) throws IOException {
		List<InputStream> iStreams = getInputStreams(files);
		this.iStream = new SequenceInputStream(Collections.enumeration(iStreams));
	}
	
	private List<InputStream> getInputStreams(List<File> files) throws IOException {
		List<InputStream> iStreams = new ArrayList<>();
		for(File f:files){
			InputStream iStream = new FileInputStream(f);
			iStreams.add(iStream);
		}
		return iStreams;
	}
	
	
	@Override
	public int read() throws IOException {
		return iStream.read();
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return iStream.read(b, off, len);
	}
	
	@Override
	public int available() throws IOException {
		return iStream.available();
	}
	
	@Override
	public void close() throws IOException {
		iStream.close();
	}
	
}
