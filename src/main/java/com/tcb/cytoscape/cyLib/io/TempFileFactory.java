package com.tcb.cytoscape.cyLib.io;

import java.io.File;
import java.io.IOException;

import com.tcb.cytoscape.cyLib.util.TempUtil;

public class TempFileFactory implements FileFactory {
	private TempUtil tempUtil;

	public TempFileFactory(TempUtil tempUtil){
		this.tempUtil = tempUtil;
	}

	@Override
	public File create() throws IOException {
		return tempUtil.createTempFile().toFile();
	}
	
	
}
