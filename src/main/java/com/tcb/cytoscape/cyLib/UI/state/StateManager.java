package com.tcb.cytoscape.cyLib.UI.state;

import java.awt.Container;
import java.util.Optional;

public interface StateManager<T extends Container> {
	public void updateState();
	public void register(T obj);
	public void reset();
	public T getRegisteredObject();
}
