package com.tcb.cytoscape.cyLib.UI.panel;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class WrapPanel<T extends JComponent> extends JPanel {
	
	private T element;
	
	public WrapPanel(T element){
		this(element, 0, 0, 0, 0);
	}
	
	public WrapPanel(T element, int topPadding, int leftPadding, int bottomPadding, int rightPadding){
		this.element = element;
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.element.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(Box.createVerticalGlue());
		this.add(this.element);
		this.add(Box.createVerticalGlue());
		this.setBorder(new EmptyBorder(topPadding,leftPadding,bottomPadding,rightPadding));
	}
	
	public T getWrappedElement(){
		return element;
	}
	
	
	
	
}
