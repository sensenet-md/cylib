package com.tcb.cytoscape.cyLib.UI.state;

import java.awt.Component;
import java.awt.Container;
import java.util.List;
import java.util.Optional;

public abstract class AbstractStateManager<T extends Container> implements StateManager<T> {
	public abstract void updateState();
	
	private Optional<T> object;
	
	public AbstractStateManager(){
		reset();
	}
	
	@Override
	public void register(T obj){
		if(object.isPresent()) throw new RuntimeException("State Manager already has registered an object");
		object = Optional.of(obj);
	}
	
	@Override
	public void reset(){
		this.object = Optional.empty();
	}
	
	@Override
	public T getRegisteredObject(){
		return object.orElseThrow(
				() -> new RuntimeException("State Manager has no registered object"));
	}
}
