package com.tcb.cytoscape.cyLib.UI;

import java.awt.Dimension;
import java.util.List;

import javax.swing.JComboBox;

public class SmallComboBox<T> extends JComboBox<T> {
	
	private static final long serialVersionUID = -4763916683392376260L;

	public SmallComboBox(){
		initSize();
	}
	
	public SmallComboBox(T[] items){
		super(items);
		initSize();
	}
	
	private void initSize(){
		this.setMaximumSize(new Dimension(200,10));
	}
	
	public void addAndSelect(T item){
		this.addItem(item);
		this.setSelectedIndex(this.getItemCount()-1);
	}
	
	public void select(int idx){
		this.setSelectedIndex(idx);
	}
	
	public void clear(){
		this.removeAllItems();
	}
	
	@Override
    public Dimension getMaximumSize() {
        Dimension max = super.getMaximumSize();
        max.height = getPreferredSize().height;
        return max;
    }
}
