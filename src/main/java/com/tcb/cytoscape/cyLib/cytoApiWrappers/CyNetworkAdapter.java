package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.subnetwork.CySubNetwork;

import com.tcb.cytoscape.cyLib.data.DefaultColumns;

import org.cytoscape.model.CyIdentifiable;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

public class CyNetworkAdapter {
	private CyNetwork network;
	
	public CyNetworkAdapter(CyNetwork network){
		if(network==null) throw new IllegalArgumentException("Network must not be null");
		this.network = network;
	}
	
	public CyNetwork getAdaptedNetwork(){
		return network;
	}
	
	public CyTableAdapter getDefaultEdgeTable() {
		CyTable table = network.getDefaultEdgeTable();
		CyTableAdapter tableAdapter = new CyTableAdapter(table);
		return tableAdapter;
	}

	public CyEdge getEdge(Long headSUID) {
		return network.getEdge(headSUID);
	}

	public Long getSUID() {
		return network.getSUID();
	}

	public CyTableAdapter getDefaultNodeTable() {
		CyTable table = network.getDefaultNodeTable();
		CyTableAdapter tableAdapter = new CyTableAdapter(table);
		return tableAdapter;
	}
	
	public CyTableAdapter getDefaultNetworkTable(){
		CyTable table = network.getDefaultNetworkTable();
		CyTableAdapter tableAdapter = new CyTableAdapter(table);
		return tableAdapter;
	}
	
	public CyTableAdapter getHiddenNetworkTable() {
		return new CyTableAdapter(network.getTable(CyNetwork.class, CyNetwork.HIDDEN_ATTRS));
	}

	public CyNode getNode(Long headSUID) {
		return network.getNode(headSUID);
	}
	
	public CyIdentifiable getNodeOrEdge(Long suid){
		CyIdentifiable result = null;
		result = getNode(suid);
		if(result!=null) return result;
		result = getEdge(suid);
		return result;
	}
	
	public Boolean contains(Long suid){
		if(getSUID().equals(suid)) return true;
		else {
			return getNodeOrEdge(suid)!=null;
		}
	}

	public List<CyEdge> getEdgeList() {
		return network.getEdgeList();
	}

	public CyRowAdapter getRow(CyIdentifiable cyId) {
		CyRow row = network.getRow(cyId);
		CyRowAdapter rowAdapter = new CyRowAdapter(row);
		return rowAdapter;
	}
	
	public CyRowAdapter getHiddenRow(CyIdentifiable cyId){
		CyRow row = network.getRow(cyId, CyNetwork.HIDDEN_ATTRS);
		return new CyRowAdapter(row);
	}

	public List<CyEdge> getConnectingEdgeList(CyNode node1, CyNode node2, Type type) {
		return network.getConnectingEdgeList(node1, node2, type);
	}
	
	public List<CyEdge> getAdjacentEdgeList(CyNode node, CyEdge.Type type){
		return network.getAdjacentEdgeList(node, type);
	}
	
	public String toString(){
		return network.toString();
	}
	
	public List<CyNode> getNeighborList(CyNode node, CyEdge.Type type){
		return network.getNeighborList(node, type);
	}
	
	public CyTableAdapter getTable(Class<? extends CyIdentifiable> networkObjectType, String tableType){
		CyTable table = network.getTable(networkObjectType,tableType);
		
		return new CyTableAdapter(table);
	}
	
	public List<CyNode> getNodeList(){
		return network.getNodeList();
	}
	
	public List<CyNode> getSelectedNodes(){
		List<CyNode> selectedNodes = new ArrayList<CyNode>();
		for(CyNode node:this.getNodeList()){
			CyRowAdapter row = this.getRow(node);
			if(row.get(DefaultColumns.SELECTED,Boolean.class)==true){
				selectedNodes.add(node);
			}
		}
		return selectedNodes;
	}
	
	public List<CyEdge> getSelectedEdges(){
		return this.getEdgeList().stream()
				.filter(e -> this.getRow(e).get(DefaultColumns.SELECTED, Boolean.class).equals(true))
				.collect(Collectors.toList());
	}
	
	public CyNode addNode(){
		return network.addNode();
	}
	
	public boolean restoreNode(CyNode node){
		return getSubNetwork().addNode(node);
	}
	
	public CyEdge addEdge(CyNode source, CyNode target, boolean isDirected){
		return network.addEdge(source, target, isDirected);
	}
	
	public boolean restoreEdge(CyEdge edge){
		return getSubNetwork().addEdge(edge);
	}
	
	public boolean removeNodes(Collection<CyNode> nodes){
		return network.removeNodes(nodes);
	}
	
	public boolean containsNode(CyNode node){
		return network.containsNode(node);
	}
	
	public boolean containsEdge(CyNode source, CyNode target){
		return network.containsEdge(source, target);
	}
	
	public boolean containsSourceAndTarget(CyEdge edge){
		return containsNode(edge.getSource()) && containsNode(edge.getTarget());
	}
	
	public void removeEdges(Collection<CyEdge> edges){
		network.removeEdges(edges);
	}
	
	public List<CyEdge> getNeighborEdges(CyEdge edge){
		Set<CyEdge> neighbors = new HashSet<>();
		neighbors.addAll(getAdjacentEdgeList(edge.getSource(),CyEdge.Type.ANY));
		neighbors.addAll(getAdjacentEdgeList(edge.getTarget(),CyEdge.Type.ANY));
		neighbors.remove(edge);
		return new ArrayList<>(neighbors);
	}
		
	private CySubNetwork getSubNetwork(){
		return (CySubNetwork)network;
	}
	
}
