package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.List;
import java.util.stream.Collectors;

import org.cytoscape.model.CyIdentifiable;
import org.cytoscape.session.CySession;

public class CySessionAdapter {

	private CySession session;

	public CySessionAdapter(CySession session){
		this.session = session;
	}
	
	public CySession getAdapted(){
		return session;
	}
	
	public List<CyNetworkAdapter> getNetworks(){
		return session.getNetworks().stream()
				.map(n -> new CyNetworkAdapter(n))
				.collect(Collectors.toList());
	}
	
	public <T extends CyIdentifiable> T getObject(Long oldSUID, Class<T> type){
		return session.getObject(oldSUID, type);
	}
	
	public <T extends CyIdentifiable> Long getUpdatedSUID(Long oldSUID, Class<T> type){
		return this.getObject(oldSUID, type).getSUID();
	}
	
}
