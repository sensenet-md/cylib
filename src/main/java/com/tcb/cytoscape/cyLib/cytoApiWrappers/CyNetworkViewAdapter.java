package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;

import java.util.List;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;

import com.tcb.cytoscape.cyLib.view.ViewPoint;

public class CyNetworkViewAdapter {
	private CyNetworkView view;
	
	public CyNetworkViewAdapter(CyNetworkView view){
		if(view==null) throw new IllegalArgumentException("View must not be null");
		this.view = view;
	}
	
	public ViewPoint getViewPoint(){
		return new ViewPoint(
				this.view.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION),
				this.view.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION),
				this.view.getVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR));
	}
	
	public void restoreViewPoint(ViewPoint viewPoint){
		this.view.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, viewPoint.getX());
		this.view.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION, viewPoint.getY());
		this.view.setVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR, viewPoint.getScaling());
	}

	public List<View<CyNode>> getNodeViews() {
		return new ArrayList<View<CyNode>>(view.getNodeViews());
	}
	
	public void updateView(){
		view.updateView();
	}

	public View<CyNode> getNodeView(CyNode node) {
		return view.getNodeView(node);
	}
	
	public CyNetworkView getAdaptedNetworkView(){
		return view;
	}
	
	public <T> T getVisualProperty(VisualProperty<T> p){
		return view.getVisualProperty(p);
	}
	
	public View<CyEdge> getEdgeView(CyEdge edge){
		return view.getEdgeView(edge);
	}
	
}
