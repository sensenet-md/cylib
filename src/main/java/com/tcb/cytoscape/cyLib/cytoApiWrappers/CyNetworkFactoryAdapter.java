package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import org.cytoscape.model.CyNetworkFactory;

public class CyNetworkFactoryAdapter {
	private CyNetworkFactory fac;

	public CyNetworkFactoryAdapter(CyNetworkFactory fac){
		this.fac = fac;
		
	}
	
	public CyNetworkAdapter createNetwork(){
		return new CyNetworkAdapter(fac.createNetwork());
	}
	
}
