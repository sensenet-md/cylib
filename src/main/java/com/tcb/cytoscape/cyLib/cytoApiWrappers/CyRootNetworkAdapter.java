package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.SavePolicy;
import org.cytoscape.model.subnetwork.CyRootNetwork;
import org.cytoscape.model.subnetwork.CySubNetwork;

public class CyRootNetworkAdapter extends CyNetworkAdapter {

	private CyRootNetwork rootNetwork;

	public CyRootNetworkAdapter(CyRootNetwork network) {
		super(network);
		this.rootNetwork = network;
		
	}
	
	public CyTableAdapter getSharedNodeTable(){
		return new CyTableAdapter(rootNetwork.getSharedNodeTable());
	}
	
	public CyTableAdapter getSharedEdgeTable(){
		return new CyTableAdapter(rootNetwork.getSharedEdgeTable());
	}
	
	public List<CyNetworkAdapter> getSubNetworkList(){
		return rootNetwork.getSubNetworkList().stream()
				.map(n -> new CyNetworkAdapter(n))
				.collect(Collectors.toList());
	}
	
	public CyNetworkAdapter addSubNetwork(Iterable<CyNode> nodes,
                           Iterable<CyEdge> edges){
		return new CyNetworkAdapter(rootNetwork.addSubNetwork(nodes,edges));
	}
	
	public CyNetworkAdapter addSubNetwork(Iterable<CyNode> nodes,
            Iterable<CyEdge> edges, SavePolicy savePolicy){
		return new CyNetworkAdapter(rootNetwork.addSubNetwork(nodes,edges,savePolicy));
	}
	
	public void removeSubNetwork(CyNetworkAdapter network) {
		rootNetwork.removeSubNetwork((CySubNetwork) network.getAdaptedNetwork());
	}
	
	public List<CyEdge> getDeletedEdges(CyNetworkAdapter network){
		Set<Long> currentEdgeSuids = network.getEdgeList().stream()
				.map(e -> e.getSUID())
				.collect(Collectors.toSet());
		return this.getEdgeList().stream()
				.filter(e -> !currentEdgeSuids.contains(e.getSUID()))
				.filter(e -> network.containsSourceAndTarget(e))
				.collect(Collectors.toList());
	}	
}
