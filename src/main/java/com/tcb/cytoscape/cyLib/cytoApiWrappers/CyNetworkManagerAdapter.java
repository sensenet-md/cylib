package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.model.CyNetworkManager;

public class CyNetworkManagerAdapter {
	private CyNetworkManager mgr;

	public CyNetworkManagerAdapter(CyNetworkManager mgr){
		this.mgr = mgr;
	}
	
	public void addNetwork(CyNetworkAdapter network){
		mgr.addNetwork(network.getAdaptedNetwork());
	}
	
	public void destroyNetwork(CyNetworkAdapter network){
		mgr.destroyNetwork(network.getAdaptedNetwork());
	}
	
	public Set<CyNetworkAdapter> getNetworkSet(){
		return mgr.getNetworkSet().stream()
				.map(n -> new CyNetworkAdapter(n))
				.collect(Collectors.toSet());
	}
	
}
