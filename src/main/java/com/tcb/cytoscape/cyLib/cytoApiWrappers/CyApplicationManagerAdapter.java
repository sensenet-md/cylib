package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkViewAdapter;

public class CyApplicationManagerAdapter {
	private CyApplicationManager mgr;

	public CyApplicationManagerAdapter(CyApplicationManager mgr){
		this.mgr = mgr;
	}

	public CyNetworkAdapter getCurrentNetwork() {
		CyNetwork network = mgr.getCurrentNetwork();
		CyNetworkAdapter networkAdapter = new CyNetworkAdapter(network);
		return networkAdapter;
	}
	
	public Boolean hasCurrentNetwork(){
		CyNetwork network = mgr.getCurrentNetwork();
		if(network==null) return false;
		else return true;
	}
	
	public CyNetworkViewAdapter getCurrentNetworkView(){
		CyNetworkView view = mgr.getCurrentNetworkView();
		CyNetworkViewAdapter viewAdapter = new CyNetworkViewAdapter(view);
		return viewAdapter;
	}
	
	public Boolean hasCurrentNetworkView(){
		CyNetworkView view = mgr.getCurrentNetworkView();
		if(view==null) return false;
		else return true;
	}
	
	
}
