package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.io.File;

import org.cytoscape.task.write.ExportNetworkTaskFactory;
import org.cytoscape.work.TaskIterator;

public class ExportNetworkTaskFactoryAdapter {
	private ExportNetworkTaskFactory fac;

	public ExportNetworkTaskFactoryAdapter(ExportNetworkTaskFactory fac){
		this.fac = fac;
		
	}
	
	public TaskIterator createTaskIterator(CyNetworkAdapter network, File file){
		return fac.createTaskIterator(network.getAdaptedNetwork(), file);
	}
}
