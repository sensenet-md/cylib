package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.Collection;

import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewManager;

public class CyNetworkViewManagerAdapter {
	private CyNetworkViewManager mgr;

	public CyNetworkViewManagerAdapter(CyNetworkViewManager mgr){
		this.mgr = mgr;
	}

	public Collection<CyNetworkView> getNetworkViews(CyNetworkAdapter network) {
		return mgr.getNetworkViews(network.getAdaptedNetwork());
	}

	public void destroyNetworkView(CyNetworkView view) {
		mgr.destroyNetworkView(view);
		
	}

	public void addNetworkView(CyNetworkView view) {
		mgr.addNetworkView(view);
		
	}
	
	
}
