package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableFactory;

public class CyTableFactoryAdapter {
	private CyTableFactory fac;

	public CyTableFactoryAdapter(CyTableFactory fac){
		this.fac = fac;
	}

	public <T> CyTableAdapter createTable(String tableName, String string, Class<T> class1, boolean b, boolean c) {
		CyTable table = this.fac.createTable(tableName, string, class1, b, c);
		CyTableAdapter tableAdapter = new CyTableAdapter(table);
		return tableAdapter;
	}
	
	
}
