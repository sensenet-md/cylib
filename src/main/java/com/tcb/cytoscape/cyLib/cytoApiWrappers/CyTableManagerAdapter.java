package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;

import com.tcb.common.util.ListFilter;
import com.tcb.common.util.OptionalUtil;

public class CyTableManagerAdapter {
	private CyTableManager mgr;

	public CyTableManagerAdapter(CyTableManager mgr){
		this.mgr = mgr;
	}
	
	public void addTable(CyTableAdapter table) {
		this.mgr.addTable(table.getAdaptedTable());
		
	}
	
	public void deleteTable(Long suid){
		this.mgr.deleteTable(suid);
		
	}
	
	public CyTableAdapter getTable(Long suid){
		
		CyTable table = mgr.getTable(suid);
		
		CyTableAdapter tableAdapter = new CyTableAdapter(table);
		return tableAdapter;
	}
	
	public Optional<CyTableAdapter> getTable(String title){
		Optional<CyTable> table = ListFilter.singleton(mgr.getAllTables(true).stream()
				.filter(t -> t.getTitle().equals(title))
				.collect(Collectors.toList()));
		return OptionalUtil.applyIfPresent(table,CyTableAdapter::new);
		
	}
	
	public List<String> getGlobalTableTitles(){
		List<String> titles = new ArrayList<String>();
			for(CyTableAdapter table:getGlobalTables()){
				String title = table.getTitle();
				titles.add(title);
		}
		return titles;
	}
	
	public List<CyTableAdapter> getGlobalTables(){
		Set<CyTable> tables = mgr.getGlobalTables();
		List<CyTableAdapter> tableAdapters = new ArrayList<CyTableAdapter>();
		for(CyTable table:tables){
			CyTableAdapter tableAdapter = new CyTableAdapter(table);
			tableAdapters.add(tableAdapter);
		}
		return tableAdapters;
	}
}
