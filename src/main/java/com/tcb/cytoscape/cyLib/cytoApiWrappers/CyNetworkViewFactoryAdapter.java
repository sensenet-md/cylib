package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;

public class CyNetworkViewFactoryAdapter {
	private CyNetworkViewFactory factory;

	public CyNetworkViewFactoryAdapter(CyNetworkViewFactory factory){
		this.factory = factory;
	}

	public CyNetworkView createNetworkView(CyNetworkAdapter network) {
		return factory.createNetworkView(network.getAdaptedNetwork());
	}
}
