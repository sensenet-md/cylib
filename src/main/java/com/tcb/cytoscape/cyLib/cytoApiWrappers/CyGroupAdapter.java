package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.List;

import org.cytoscape.event.CyEventHelper;
import org.cytoscape.group.CyGroup;
import org.cytoscape.model.CyNode;

public class CyGroupAdapter {
	private CyGroup group;
	private CyRowAdapter headRow;
	

	public CyGroupAdapter(CyGroup group){
		this.group = group;
	}
	
	public CyGroup getAdaptedGroup(){
		return this.group;
	}
	

	public void collapse(CyNetworkAdapter network) {
		group.collapse(network.getAdaptedNetwork());
	}
	
	public void expand(CyNetworkAdapter network) {
		group.expand(network.getAdaptedNetwork());
	}
	
	public void addNodes(List<CyNode> subNodes) {
		group.addNodes(subNodes);
		
	}
	
	public boolean isCollapsed(CyNetworkAdapter network){
		return group.isCollapsed(network.getAdaptedNetwork());
	}
	
	public CyNode getGroupNode(){
		return group.getGroupNode();
	}
	
	public List<CyNode> getNodeList(){
		return group.getNodeList();
	}
	
	
	
	
}
