package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;
import java.util.List;

import org.cytoscape.model.CyNode;
import org.cytoscape.view.layout.LayoutNode;
import org.cytoscape.view.model.View;

public class LayoutNodeAdapter {
	private LayoutNode layoutNode;

	public LayoutNodeAdapter(LayoutNode layoutNode){
		this.layoutNode = layoutNode;
	}

	public static LayoutNodeAdapter createFrom(View<CyNode> nodeV, int indexOf, CyRowAdapter nodeRow) {
		LayoutNode newNode = new LayoutNode(nodeV,indexOf,nodeRow.getAdaptedRow());
		LayoutNodeAdapter newNodeAdapter = new LayoutNodeAdapter(newNode);
		return newNodeAdapter;
	}

	public void setLocation(double dx, double dy) {
		layoutNode.setLocation(dx, dy);
		
	}

	public void moveToLocation() {
		layoutNode.moveToLocation();
		
	}

	public double getX() {
		return layoutNode.getX();
	}

	public double getY() {
		return layoutNode.getY();
	}

	public void increment(double x, double y) {
		layoutNode.increment(x, y);
		
	}

	public void decrement(double x, double y) {
		layoutNode.decrement(x, y);
		
	}

	public CyNode getNode() {
		return layoutNode.getNode();
	}
	
	public List<LayoutNodeAdapter> getNeighbors(){
		List<LayoutNodeAdapter> neighborsAdapted = new ArrayList<LayoutNodeAdapter>();
		for(LayoutNode node: layoutNode.getNeighbors()){
			neighborsAdapted.add(new LayoutNodeAdapter(node));
		}
		return neighborsAdapted;
	}
	
	public void lock(){
		layoutNode.lock();
	}
	
	public void unLock(){
		layoutNode.unLock();
	}

	public void setX(Double x) {
		layoutNode.setX(x);
	}
	
	public void setY(Double y) {
		layoutNode.setY(y);
	}
}
