package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import org.cytoscape.model.subnetwork.CyRootNetwork;
import org.cytoscape.model.subnetwork.CyRootNetworkManager;

public class CyRootNetworkManagerAdapter {
	private CyRootNetworkManager mgr;

	public CyRootNetworkManagerAdapter(CyRootNetworkManager mgr){
		this.mgr = mgr;
	}
	
	public CyRootNetworkAdapter getRootNetwork(CyNetworkAdapter network){
		CyRootNetwork rootNetwork = mgr.getRootNetwork(network.getAdaptedNetwork());
		return new CyRootNetworkAdapter(rootNetwork);
	}
}
