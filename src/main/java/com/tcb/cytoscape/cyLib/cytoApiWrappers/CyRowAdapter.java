package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.cytoscape.model.CyRow;

import com.tcb.cytoscape.cyLib.data.Columns;
import com.tcb.cytoscape.cyLib.errors.InvalidColumnException;

public class CyRowAdapter {
	
	private CyRow row;
	
	public CyRowAdapter(CyRow row){
		this.row = row;
	}
	
	public CyRow getAdaptedRow(){
		return row;
	}
	
	public void set(Columns colType, Object value) {
		row.set(colType.toString(), value);
	}

	public Map<String, Object> getAllValues() {
		return row.getAllValues();
	}
	
	public <ColumnType> Optional<ColumnType> getRawMaybe(String columnName, Class<ColumnType> returnType){
		if(!contains(columnName)) throw new InvalidColumnException("Invalid column");
		return Optional.ofNullable(row.get(columnName, returnType));
	}
	
	public <ColumnType> ColumnType getRaw(String columnName, Class<ColumnType> returnType){
		Optional<ColumnType> result = getRawMaybe(columnName,returnType);
		return result.orElseThrow( () -> new InvalidColumnException("No value set in row."));
	}
	
	public <ColumnType> Optional<List<ColumnType>> getRawListMaybe(String columnName, Class<ColumnType> returnType){
		if(!contains(columnName)) throw new InvalidColumnException("Invalid column");
		return Optional.ofNullable(row.getList(columnName, returnType));
	}
	
	public <ColumnType> List<ColumnType> getRawList(String colType, Class<ColumnType> returnType){
		Optional<List<ColumnType>> result = getRawListMaybe(colType,returnType);
		return result.orElseThrow( () -> new InvalidColumnException("No value set in row."));
	}
	
	public <ColumnType> ColumnType get(Columns colType, Class<ColumnType> returnType){
		return getRaw(colType.toString(),returnType);
	}
	
	public <ColumnType> Optional<ColumnType> getMaybe(Columns columnName, Class<ColumnType> returnType){
		return getRawMaybe(columnName.toString(),returnType);
	}
	
	public <ColumnType> Optional<List<ColumnType>> getListMaybe(Columns columnName, Class<ColumnType> returnType){
		return getRawListMaybe(columnName.toString(),returnType);
	}
	
	public <ColumnType> List<ColumnType> getList(Columns colType, Class<ColumnType> returnType){
		return getRawList(colType.toString(),returnType);
	}
		
	public CyTableAdapter getTable(){
		return new CyTableAdapter(row.getTable());
	}
	
	public void clear(Columns column){
		this.set(column, null);
	}
	
	public Boolean contains(String columnName){
		return getTable().columnExists(columnName);
	}
				
}
