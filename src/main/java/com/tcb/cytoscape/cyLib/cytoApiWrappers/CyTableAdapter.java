package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Map.Entry;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import com.tcb.cytoscape.cyLib.data.Columns;
import com.tcb.cytoscape.cyLib.errors.InvalidColumnException;

public class CyTableAdapter {
	private CyTable table;

	public CyTableAdapter(CyTable table){
		this.table = table;
	}
	
	public CyTable getAdaptedTable(){
		return table;
	}

	public CyRowAdapter getRow(Object primaryKey) {
		CyRow row = table.getRow(primaryKey);
		CyRowAdapter rowAdapter = new CyRowAdapter(row);
		return rowAdapter;
	}

	public CyColumn getColumn(Columns key) throws InvalidColumnException {
		Optional<CyColumn> column = Optional.ofNullable(table.getColumn(key.toString()));
		return column.orElseThrow( () -> new InvalidColumnException(
				String.format("Column does not exist: %s",key)));
	}

	public void deleteRows(List<Object> objects) {
		table.deleteRows(objects);
		
	}

	public List<CyRowAdapter> getAllRows() {
		List<CyRow> rows = table.getAllRows();
		List<CyRowAdapter> rowAdapters = new ArrayList<CyRowAdapter>();
		for(CyRow row:rows){
			CyRowAdapter rowAdapter = new CyRowAdapter(row);
			rowAdapters.add(rowAdapter);
		}
		return rowAdapters;
	}

	public Collection<CyColumn> getColumns() {
		return table.getColumns();
	}

	public void createColumn(Columns name, Class<?> type, boolean isImmutable) {
		String col = name.toString();
		if(!columnExists(col))
			table.createColumn(name.toString(), type, isImmutable);
	}
	
	public void createListColumn(Columns name, Class<?> type, boolean isImmutable){
		String col = name.toString();
		if(!columnExists(col))
			table.createListColumn(name.toString(), type, isImmutable);
	}
	
	public void addVirtualColumn(Columns name, CyTableAdapter sourceTable, Columns joinKey, boolean isImmutable){
		table.addVirtualColumn(name.toString(), name.toString(), sourceTable.getAdaptedTable(), joinKey.toString(), isImmutable);
	}
	
	public Long getSUID(){
		return table.getSUID();
	}
	
	public String getTitle(){
		return table.getTitle();
	}
	
	public void deleteColumn(Columns columnName){
		table.deleteColumn(columnName.toString());
	}
	
	public void clearColumn(Columns columnName){
		List<CyRowAdapter> rows = getAllRows();
		for(CyRowAdapter row:rows){
			row.set(columnName, null);
		}
	}
	
	public Boolean columnExists(Columns columnName){
		return columnExists(columnName.toString());
	}
	
	public Boolean columnExists(String columnName){
		return table.getColumn(columnName)!=null;
	}
	
	public boolean deleteRows(Collection<?> primaryKeys){
		return table.deleteRows(primaryKeys);
	}
	
	
}
