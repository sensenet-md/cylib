package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import org.cytoscape.group.CyGroup;
import org.cytoscape.group.CyGroupFactory;
import org.cytoscape.model.CyNode;

public class CyGroupFactoryAdapter {
	private CyGroupFactory factory;

	public CyGroupFactoryAdapter(CyGroupFactory factory){
		this.factory = factory;
	}

	public CyGroupAdapter createGroup(CyNetworkAdapter network, CyNode headNode, boolean register) {
		CyGroup group = factory.createGroup(network.getAdaptedNetwork(), headNode, register);
		CyGroupAdapter groupAdapter = new CyGroupAdapter(group);
		return groupAdapter;
	}
	
	public CyGroupAdapter createGroup(CyNetworkAdapter network, boolean register){
		CyGroup group = factory.createGroup(network.getAdaptedNetwork(), register);
		return new CyGroupAdapter(group);
	}

	
}
