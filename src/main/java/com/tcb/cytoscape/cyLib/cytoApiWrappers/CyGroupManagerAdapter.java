package com.tcb.cytoscape.cyLib.cytoApiWrappers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.group.CyGroup;
import org.cytoscape.group.CyGroupManager;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;

import com.tcb.common.exception.NoMatchesException;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.OptionalUtil;

public class CyGroupManagerAdapter{
	private CyGroupManager mgr;

	public CyGroupManagerAdapter(CyGroupManager mgr){
		this.mgr = mgr;
	}
		
	public boolean isGroup(CyNode headNode, CyNetworkAdapter network) {
		return mgr.isGroup(headNode,network.getAdaptedNetwork());
	}

	public Optional<CyGroupAdapter> getGroup(CyNode headNode, CyNetworkAdapter network) {
		Optional<CyGroup> group = Optional.ofNullable(mgr.getGroup(headNode, network.getAdaptedNetwork()));
		return OptionalUtil.applyIfPresent(group, CyGroupAdapter::new);
	}

	public void destroyGroup(CyGroupAdapter group) {
		mgr.destroyGroup(group.getAdaptedGroup());
	}
	
	
	

	public void addGroup(CyGroupAdapter group) {
		mgr.addGroup(group.getAdaptedGroup());
		
	}

	public Set<CyGroupAdapter> getGroupSet(CyNetworkAdapter network) {
		Set<CyGroup> groupSet = mgr.getGroupSet(network.getAdaptedNetwork());
		Set<CyGroupAdapter> groupAdapterSet = new HashSet<CyGroupAdapter>();
		for(CyGroup group:groupSet){
			CyGroupAdapter groupAdapter = new CyGroupAdapter(group);
			groupAdapterSet.add(groupAdapter);
		}
		return groupAdapterSet;
	}
	
	public void reset(){
		mgr.reset();
	}
	
	public CyGroupAdapter getGroupForNode(CyNode node, CyNetworkAdapter network) throws IllegalArgumentException {
		Optional<CyGroup> group = ListFilter.singleton(mgr.getGroupsForNode(node, network.getAdaptedNetwork()));
		if(!group.isPresent()){
			group = Optional.ofNullable(mgr.getGroup(node, network.getAdaptedNetwork()));
		}
		if(!group.isPresent()){
			throw new IllegalArgumentException(String.format("Could not find group for node: %d",node.getSUID()));
		}
		return new CyGroupAdapter(group.get());
	}

	public CyNode getGroupNodeOrKeepNode(CyNode node, CyNetworkAdapter network){
		if(isGroup(node, network))
			return getGroupForNode(node, network).getGroupNode();
		else
			return node;
	}
	
	public List<CyEdge> getConnectingSubEdges(CyEdge edge, CyRootNetworkAdapter rootNetwork){
		Set<CyNode> sourceNodes = getAllSubNodes(edge.getSource(),rootNetwork);
		Set<CyNode> targetNodes = getAllSubNodes(edge.getTarget(),rootNetwork);
		List<CyEdge> subEdges = rootNetwork.getEdgeList().stream()
				.filter(e -> isSubEdge(e,sourceNodes,targetNodes))
				.collect(Collectors.toList());
		return subEdges;
		}
	
	private static Boolean isSubEdge(CyEdge e, Set<CyNode> sourceNodes, Set<CyNode> targetNodes){
		CyNode source = e.getSource();
		CyNode target = e.getTarget();
		return (
				sourceNodes.contains(source) && 
				targetNodes.contains(target)
				) ||
				(
				targetNodes.contains(source) &&
				sourceNodes.contains(target));
	}
	
	private Set<CyNode> getAllSubNodes(CyNode node, CyRootNetworkAdapter rootNetwork){
		Set<CyNode> nodes = new HashSet<CyNode>();
		if(isGroup(node,rootNetwork))
			nodes.addAll(getGroupForNode(node,rootNetwork).getNodeList());
		else
			nodes.add(node);
		return nodes;
	}
}
