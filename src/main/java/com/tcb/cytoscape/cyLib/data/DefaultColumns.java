package com.tcb.cytoscape.cyLib.data;

public enum DefaultColumns implements Columns {
	SUID, SELECTED, SHARED_NAME, SHARED_INTERACTION, NAME, TYPE;

	public String toString(){
		switch(this){
		case SUID: return "suid";
		case SHARED_NAME: return "shared name";
		case NAME: return "name";
		case SHARED_INTERACTION: return "shared interaction";
		case TYPE: return "type";
		case SELECTED: return "selected";
		default: throw new IllegalArgumentException("Unknown enum");
		}
	}
}
