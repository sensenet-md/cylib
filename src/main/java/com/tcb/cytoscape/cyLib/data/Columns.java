package com.tcb.cytoscape.cyLib.data;

public interface Columns {

	public String name();
	public String toString();
	
	public default String toShortString(){
		return toString();
	}
	
}