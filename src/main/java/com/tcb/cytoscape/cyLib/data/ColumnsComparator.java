package com.tcb.cytoscape.cyLib.data;

import java.util.Comparator;

public class ColumnsComparator implements Comparator<Columns> {

	@Override
	public int compare(Columns o1, Columns o2) {
		return o1.name().compareTo(o2.name());
	}

}
