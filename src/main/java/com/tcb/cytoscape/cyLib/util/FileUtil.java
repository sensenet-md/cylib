package com.tcb.cytoscape.cyLib.util;

import java.io.File;
import java.io.IOException;

public class FileUtil {
	public static void deleteDirectory(File directory) throws IOException {
		if(!directory.exists()) return;
		File[] files = directory.listFiles();
		// If directory is a file, then files is null
		if(files!=null) {
			for(int i=0;i<files.length;i++){
				File file = files[i];
				if(file.isDirectory()) deleteDirectory(file);
				else file.delete();
			}
		}
		boolean deleted = directory.delete();
		if(!deleted) throw new IOException("Could not delete directory: " + directory.getAbsolutePath());
		return;
	}
}
