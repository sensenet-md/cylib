package com.tcb.cytoscape.cyLib.util;

import java.util.Objects;

public class NullUtil {
	public static void requireNonNull(Object o, String name){
		Objects.requireNonNull(o, name + " may not be null");
	}
}
