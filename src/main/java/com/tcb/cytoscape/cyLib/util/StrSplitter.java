package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StrSplitter {
	public static List<Character> splitToChar(String s){
		List<Character> result = new ArrayList<Character>();
		char[] sChars = s.toCharArray();
		for(char c: sChars){
			result.add(c);
		}
		return result;
	}
		
	public static List<Integer> splitToInt(String s){
		return splitToChar(s).stream()
				.map(c -> c.toString())
				.map(str -> Integer.parseInt(str))
				.collect(Collectors.toList());
	}
}
