package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CyclingIteratorImpl<T> implements CyclingIterator<T> {
	private List<T> iterables;
	private int maxIndex;
	private int nextIndex;
	
	public CyclingIteratorImpl(Collection<T> iterables){
		this.iterables = new ArrayList<T>(iterables);
		this.maxIndex = iterables.size()-1;
		this.nextIndex = 0;
	}
	

	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public T next() {
		if(nextIndex<=maxIndex){
			T result = iterables.get(nextIndex);
			nextIndex+=1;
			return result;
		}else{
			T result = iterables.get(0);
			nextIndex=1;
			return result;
		}
	}
	
	@Override
	public Integer getCycleLength(){
		return maxIndex+1;
	}

}
