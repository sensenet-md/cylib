package com.tcb.cytoscape.cyLib.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

public class BundleUtil {
	
	private BundleContext bc;

	public BundleUtil(BundleContext bc){
		this.bc = bc;
	}
		
	public void register(Object object){
		bc.registerService(object.getClass().getName(), object, new Properties());
	}
	
	public Path resourceToPath(String pathString){
		Bundle bundle = bc.getBundle();
		URL tmpFileUrl = bundle.getResource(pathString);
		File tmpFile = bundle.getBundleContext().getDataFile(pathString);
		tmpFile.deleteOnExit();
		try {
			FileUtils.copyURLToFile(tmpFileUrl, tmpFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tmpFile.toPath();
	}
	
}
