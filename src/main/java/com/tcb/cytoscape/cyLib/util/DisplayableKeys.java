package com.tcb.cytoscape.cyLib.util;

import java.util.Set;

public interface DisplayableKeys {
	public Set<String> getDisplayedKeys();
}
