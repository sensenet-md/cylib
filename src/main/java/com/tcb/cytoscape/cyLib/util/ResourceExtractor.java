package com.tcb.cytoscape.cyLib.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class ResourceExtractor {
	
	private final String tempPrefix;

	public ResourceExtractor(String tempPrefix){
		this.tempPrefix = tempPrefix;
	}
	
	public ResourceExtractor(){
		this("tmp");
	}
	
	public void extract(Path resourcePath, Path target) throws IOException {
		List<String> subPaths = new ArrayList<>();
		for(Path p:resourcePath){
			subPaths.add(p.toString());
		}
		String normalResourcePath = String.join("/", subPaths);
		InputStream in = getClass().getClassLoader().getResourceAsStream(normalResourcePath);
		Files.copy(in, target, StandardCopyOption.REPLACE_EXISTING);
	}
	
	public Path extractTemp(Path resourcePath) throws IOException {
		File tempFile = File.createTempFile(tempPrefix, ".tmp");
		tempFile.deleteOnExit();
		extract(resourcePath, tempFile.toPath());
		return tempFile.toPath();
	}
	
	public Path extractToTempDir(Path resourcePath, String fileName) throws IOException {
		Path tempDir = new TempUtil(tempPrefix).createTempDir();
		Path tempFile = Paths.get(tempDir.toString(), fileName);
		tempFile.toFile().createNewFile();
		extract(resourcePath,tempFile);
		return tempFile;
	}
		
	
}
