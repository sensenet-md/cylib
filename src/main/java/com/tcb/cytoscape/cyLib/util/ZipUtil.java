package com.tcb.cytoscape.cyLib.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;

public class ZipUtil {

	private static final int archiveBytes = 0x504B0304;
	private static final int emptyArchiveBytes = 0x504B0506;
	private static final int spannedArchiveBytes = 0x504B0708; 
	
	
	public static InputStream decompressSingleFile(ZipFile zipFile) throws IOException {
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		ZipEntry entry = entries.nextElement();
		if(entries.hasMoreElements()) throw new IOException("zip contains more than one file");
		return zipFile.getInputStream(entry);
	}
	
	public static Boolean isArchive(Path path) throws IOException {
		RandomAccessFile raf = new RandomAccessFile(path.toFile(), "r");
		int n = raf.readInt();
		raf.close();
		Boolean result = n == archiveBytes || n == emptyArchiveBytes || n == spannedArchiveBytes;
		return result;
	}
	
	public static void decompress(Path zipFile, Path targetDir) throws IOException {
		InputStream in = new FileInputStream(zipFile.toFile());
		ArchiveInputStream ain = null;
		
		try {
			ain = createArchiveInputStream(in);
		} catch(ArchiveException e){
			in.close();
			throw new IOException(e);
		}
		
		ArchiveEntry entry = ain.getNextEntry();
		while(entry!=null){
			File outputFile = new File(targetDir.toString(),entry.getName());
			
			if(entry.isDirectory()) extractDir(outputFile);
			else extractFile(ain,outputFile);
			
			entry = ain.getNextEntry();
		}
		ain.close();
		in.close();
	}
	
	private static ArchiveInputStream createArchiveInputStream(InputStream in) throws IOException,ArchiveException {
			ArchiveInputStream ain = new ArchiveStreamFactory()
					.createArchiveInputStream(ArchiveStreamFactory.ZIP,in);
			return ain;
	}
	
	private static void extractDir(File f){
		f.mkdir();
	}
	
	private static void extractFile(ArchiveInputStream ain, File outputFile) throws IOException {
		OutputStream os = new FileOutputStream(outputFile);
		IOUtils.copy(ain,os);
		os.close();
	}
	
	public static void compressSingleFile(Path file, Path zipPath) throws IOException,ArchiveException {
		FileOutputStream fout = new FileOutputStream(zipPath.toFile()); 
		ArchiveOutputStream out = new ArchiveStreamFactory()
				.createArchiveOutputStream(ArchiveStreamFactory.ZIP, fout);
		try{
			String entryName = zipPath.getFileName().toString();
			ArchiveEntry entry = out.createArchiveEntry(file.toFile(), entryName);
			out.putArchiveEntry(entry);
			if(file.toFile().isFile()){
				InputStream i = Files.newInputStream(file);
				IOUtils.copy(i, out);
			} else {
				throw new UnsupportedOperationException("Zipping directories not supported yet");
			}
			out.closeArchiveEntry();
			out.finish();
		} finally {
			fout.close();
			out.close();
		}		
	}
}
