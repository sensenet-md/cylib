package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PiCyclingIterator extends CyclingIteratorImpl<Double> implements Iterator<Double> {

	public PiCyclingIterator(Integer desiredLength) {
		super(createIterables(desiredLength));
	}
	
	private static Collection<Double> createIterables(Integer desiredLength){
		Collection<Double> iterables = new ArrayList<Double>();
		if(desiredLength<1){
			throw new IllegalArgumentException("Iterator length must be at least 1.");
			};
		Double coefficient = 2d / (desiredLength);
		for(int i=0;i<desiredLength;i++){
			iterables.add(i*coefficient*Math.PI);
		}
		return iterables;
	}
	
	

}
