package com.tcb.cytoscape.cyLib.util;

import java.util.Map;
import java.util.function.Function;

import com.tcb.common.util.SafeMap;

public class MapUtil {
	
	public static <K,V> Map<K,V> createMap(Iterable<V> values, Function<V,K> keyFun){
		SafeMap<K,V> result = new SafeMap<>();
		for(V value:values){
			K key = keyFun.apply(value);
			result.put(key, value);
		}
		return result;
	}
	
	public static <K1,K2,V> Map<K1,Map<K2,V>> create2DMap(
			Iterable<V> values,
			Function<V,K1> keyFun1,
			Function<V,K2> keyFun2
			){
		SafeMap<K1,Map<K2,V>> result = new SafeMap<>();
		for(V value:values){
			K1 key1 = keyFun1.apply(value);
			K2 key2 = keyFun2.apply(value);
			if(!result.containsKey(key1)) result.put(key1, new SafeMap<>());
			Map<K2,V> subMap = result.get(key1);
			subMap.put(key2, value);
		}
		return result;
	}
}
