package com.tcb.cytoscape.cyLib.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.tcb.cytoscape.cyLib.util.DisplayableKeys;
import com.tcb.common.util.SafeMap;

public abstract class AutoKeyMap<V> implements DisplayableKeys, Serializable {
	private static final long serialVersionUID = -135789684561281323L;
	private static final Integer startIndex = 1;
	private Integer nextIndex = startIndex;
	private SafeMap<String, V> map;
		
	public abstract String getBase();
	
	public AutoKeyMap(){
		this.map = new SafeMap<String,V>();
	}
	
	public synchronized String putAutoKey(V value){
		String key = generateKey();
		this.put(key, value);
		return key;
	}
	
	public V get(String key){
		return map.get(key);
	}

	public synchronized V put(String key, V value){
		nextIndex++;
		return map.put(key, value);
	}
	
	public synchronized void putAll(Map<String,V> map){
		for(Entry<String,V> e:map.entrySet()){
			put(e.getKey(),e.getValue());
		}
	}
	
	private String generateKey(){
		String key = getBase() + nextIndex.toString();
		return key;	
	}

	@Override
	public Set<String> getDisplayedKeys() {
		return map.keySet();
	}
	
	public synchronized void clear(){
		map.clear();
		nextIndex = startIndex;
	}
	
	public Map<String,V> getMap(){
		return map;
	}
}
