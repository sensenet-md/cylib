package com.tcb.cytoscape.cyLib.util;

public class ProgressTicker {

	private final long steps;
	private double progress;
	private final double offset;

	public ProgressTicker(long steps){
		this.steps = steps;
		this.progress = 0d;
		this.offset = 1d / (double) steps;
	}
	
	public double incr(){
		progress+=offset;
		return progress;
	}
	
	public double get(){
		return progress;
	}
}
