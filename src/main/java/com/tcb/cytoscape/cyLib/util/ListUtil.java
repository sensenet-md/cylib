package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.tcb.common.util.ListFilter;

public class ListUtil {
	
	public static <T> Integer determineCommonElementLength(List<List<T>> list) {
		Set<Integer> lengths = list.stream()
				.map(l -> l.size())
				.collect(Collectors.toSet());
		return ListFilter.singleton(lengths).get();
		
	}
}
