package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.List;

public class Range {
	private Integer firstIndex;
	private Integer lastExcludedIndex;

	public Range(Integer firstIndex, Integer lastExcludedIndex){
		this.firstIndex = firstIndex;
		this.lastExcludedIndex = lastExcludedIndex;
	}
	
	public List<Integer> getRange(){
		List<Integer> range = new ArrayList<Integer>();
		for(int i=firstIndex;i<lastExcludedIndex;i++){
			range.add(i);
		}
		return range;
	}
	
}
