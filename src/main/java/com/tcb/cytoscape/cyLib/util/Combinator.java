package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Combinator {
	public static <T> List<List<T>> combinations(List<T> list1, List<T> list2, Comparator<? super T> comparator){
		Set<List<T>> resultSet = new HashSet<List<T>>();
		List<List<T>> resultList = new ArrayList<List<T>>();
		for(T i: list1){
			for(T j: list2){
				List<T> combination = Arrays.asList(i,j);
				Collections.sort(combination,comparator);
				resultSet.add(combination);
			}
		}
		resultList.addAll(resultSet);
		return resultList;
	}
	
	public static <T> List<List<T>> crossCombinations(List<T> list1, List<T> list2,Comparator<? super T> comparator){
		List<List<T>> resultList = new ArrayList<List<T>>();
		List<List<T>> combinations = combinations(list1,list2,comparator);
		for(List<T> combination:combinations){
			if(!combination.get(0).equals(combination.get(1))){
				resultList.add(combination);
			}
		}
		return resultList;
	}
}
