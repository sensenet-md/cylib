package com.tcb.cytoscape.cyLib.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class TempUtil {
	
	private List<Path> tempPaths = new ArrayList<>();
	private final String prefix;

	public TempUtil(String prefix){
		this(prefix, true);
	}
	
	public TempUtil(String prefix, Boolean autoClean){
		this.prefix = prefix;
		if(autoClean){
			addCleanHook();
		}
	}
	
	private void addCleanHook(){
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run(){
				clean();
			}
		});
	}
	
	public Path createTempFile() throws IOException {
		File f = File.createTempFile(prefix, ".tmp");
		tempPaths.add(f.toPath());
		return f.toPath();
	}
		
	public Path createTempDir() throws IOException {
		Path tempDir = Files.createTempDirectory(prefix);
		tempPaths.add(tempDir);
		return tempDir;
	}
	
	public synchronized void clean(){
		for(Path p: tempPaths){
			try {
				FileUtil.deleteDirectory(p.toFile());
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		tempPaths.clear();
	}
	
}
