package com.tcb.cytoscape.cyLib.util;

import java.util.ArrayList;
import java.util.List;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyRowAdapter;
import com.tcb.cytoscape.cyLib.data.Columns;
import com.tcb.cytoscape.cyLib.errors.InvalidColumnException;

public class GetFromRows {
		
	public static <ColumnType> List<ColumnType> getFromRows(List<CyRowAdapter> rows, Columns column, Class<ColumnType> columnType) throws InvalidColumnException {
		List<ColumnType> results = new ArrayList<ColumnType>();
		for(CyRowAdapter row:rows){
			ColumnType value = row.get(column, columnType);
			results.add(value);
		}
		return results;
	}
}
