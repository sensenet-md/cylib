package com.tcb.cytoscape.cyLib.util;

import java.util.Iterator;

public interface CyclingIterator<T> extends Iterator<T>{
	public Integer getCycleLength();
}
