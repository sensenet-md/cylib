package cytoApiWrappers;

import static org.junit.Assert.*;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyRowAdapter;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyTableAdapter;
import com.tcb.cytoscape.cyLib.data.Columns;
import com.tcb.cytoscape.cyLib.data.DefaultColumns;
import com.tcb.cytoscape.cyLib.errors.InvalidColumnException;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CyRowAdapterTest {
	private CyRow mockRow;
	private Columns colName;
	private CyRowAdapter rowAdapter;
	private CyTable mockTable;
	private CyColumn mockColumn;
	
	@Before
	public void setUp() throws Exception {
		mockRow = Mockito.mock(CyRow.class);
		mockTable = Mockito.mock(CyTable.class);
		mockColumn = Mockito.mock(CyColumn.class);
		colName = DefaultColumns.NAME;
		rowAdapter = new CyRowAdapter(mockRow);
		
		
		when(mockRow.getTable()).thenReturn(mockTable);
		when(mockTable.getColumn(colName.toString())).thenReturn(mockColumn);
			
	}

	@Test
	public void testGetRiskyColumnValue() {
		Double colValue = 1.0d;
		when(mockRow.get(colName.toString(), Double.class)).thenReturn(colValue);
		
		Double testColValue = rowAdapter.get(colName, Double.class);
		assertEquals(colValue,testColValue);
						
	}

	@Test(expected=RuntimeException.class)
	public void testGetRiskyColumnValueWhenInvalidColumn(){
		Double colValue = 1.0d;
		when(mockTable.getColumn(colName.toString())).thenThrow(new RuntimeException("Test"));
		
		Double testColValue = rowAdapter.get(colName, Double.class);
	}
	
}
