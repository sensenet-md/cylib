package io;

import static org.junit.Assert.*;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.io.FileFactory;
import com.tcb.cytoscape.cyLib.io.SplitFileInputStream;
import com.tcb.cytoscape.cyLib.io.SplitFileOutputStream;
import com.tcb.cytoscape.cyLib.io.TempFileFactory;
import com.tcb.cytoscape.cyLib.util.TempUtil;

public class SplitFileOutputStreamTest {

	private TempUtil tempUtil;
	private FileFactory fileFactory;
	private String string1;
	private String string2;

	@Before
	public void setUp() throws Exception {
		this.tempUtil = new TempUtil("testSplitFile",false);
		this.fileFactory = new TempFileFactory(tempUtil);
		this.string1 = "ABCDEFG";
		this.string2 = "HIJKLMN";
	}
	
	@Test
	public void testSplitFileOutputStreamWith12Bytes() throws Exception {
		SplitFileOutputStream test = new SplitFileOutputStream(fileFactory,12);
		writeTestStrings(test);
		
		List<String> testStrings = deserialize(test.getFiles());
		
		assertEquals(3,test.getFiles().size());
		assertEquals(string1,testStrings.get(0));
		assertEquals(string2,testStrings.get(1));
	}
	
	private void writeTestStrings(SplitFileOutputStream fStream) throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(fStream);
		os.writeObject(string1);
		os.writeObject(string2);
		os.flush();
		os.close();
		fStream.close();
	}
	
	@Test(expected=IOException.class)
	public void testSplitFileOutputStreamWith2Bytes() throws Exception {
		SplitFileOutputStream test = new SplitFileOutputStream(fileFactory,2);
		writeTestStrings(test);
	}
	
	private List<String> deserialize(List<File> files) throws Exception {
		InputStream iStream = new SplitFileInputStream(files);
		ObjectInputStream oStream = new ObjectInputStream(iStream);
		List<String> result = new ArrayList<>();
		while(true){
			try{
				Object o = oStream.readObject();
				result.add((String)o);
			} catch(EOFException e){
				break;
			}
		}
		
		oStream.close();
		iStream.close();
		return result;
	}
	
}
