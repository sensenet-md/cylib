package util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.util.PiCyclingIterator;

public class PiCyclingIteratorTest {

	private Collection<Double> lengthFourPiCollection;
	private List<Double> lengthThreePiCollection;

	@Before
	public void setUp() throws Exception {
		this.lengthFourPiCollection = Arrays.asList(0d,0.5*Math.PI,Math.PI,1.5*Math.PI);
		this.lengthThreePiCollection = Arrays.asList(0d,Math.PI/1.5d,Math.PI/0.75d);
	}

	@Test
	public void testhalfPiCyclingIterator() {
		Collection<Double> testPiCollection = createTestCollection(4);		
		assertEquals(lengthFourPiCollection,testPiCollection);
	}
	
	private Collection<Double> createTestCollection(Integer desiredLength){
		Iterator<Double> testPiCyclingIterator = new PiCyclingIterator(desiredLength);
		Collection<Double> testPiCollection = new ArrayList<Double>();
		for(int i=0;i<desiredLength;i++){
			testPiCollection.add(testPiCyclingIterator.next());
		}
		return testPiCollection;
	}
	
	@Test
	public void testTwoThirdsPiCyclingIterator() {
		Collection<Double> testPiCollection = createTestCollection(3);	
		assertEquals(lengthThreePiCollection,testPiCollection);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testThrowExceptionWhenDesiredLengthSmallerOne() {
		Collection<Double> testPitCollection = createTestCollection(0);
		fail("Should be unreachable.");
	}
	
	
	
	

}
