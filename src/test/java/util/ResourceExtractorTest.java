package util;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.util.ResourceExtractor;
import com.tcb.cytoscape.cyLib.util.TempUtil;

public class ResourceExtractorTest {

	private Path resourcePath;
	private ResourceExtractor extractor;

	@Before
	public void setUp() throws Exception {
		this.resourcePath = Paths.get("util","ResourceExtractorTest","a.txt");
		this.extractor = new ResourceExtractor();
	}

	@Test
	public void testExtract() throws Exception {
		Path tempFile = new TempUtil("test").createTempFile();
		extractor.extract(resourcePath, tempFile);
		assertTrue(checkTempFile(tempFile));
	}
	
	private boolean checkTempFile(Path tempFile) throws Exception {
		String line = Files.readAllLines(tempFile).get(0);
		return("abc".equals(line));
	}
	
	@Test
	public void testExtractTemp() throws Exception {
		Path tempFile = extractor.extractTemp(resourcePath);
		assertTrue(checkTempFile(tempFile));
	}

	@Test
	public void testExtractToTempDir() throws Exception {
		Path tempFile = extractor.extractToTempDir(resourcePath, "extracted.txt");
		assertTrue(checkTempFile(tempFile));
	}

}
