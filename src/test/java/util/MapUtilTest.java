package util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.util.MapUtil;
import com.tcb.common.util.SafeMap;

public class MapUtilTest {

	
	
	private Dummy dummy1;
	private Dummy dummy2;
	
	private Map<String,Dummy> refMap;
	private Map<Integer,Map<String,Dummy>> ref2DMap;
	private List<Dummy> objects;

	@Before
	public void setUp() throws Exception {
		this.dummy1 = new Dummy("a",1);
		this.dummy2 = new Dummy("b",2);
		this.objects = Arrays.asList(dummy1,dummy2);
		
		this.refMap = new SafeMap<>();
		refMap.put(dummy1.getName(), dummy1);
		refMap.put(dummy2.getName(), dummy2);
		
		this.ref2DMap = new SafeMap<>();
		ref2DMap.put(dummy1.getId(), new SafeMap<>());
		ref2DMap.put(dummy2.getId(), new SafeMap<>());
		ref2DMap.get(dummy1.getId()).put(dummy1.getName(), dummy1);
		ref2DMap.get(dummy2.getId()).put(dummy2.getName(), dummy2);
	}

	@Test
	public void testCreateMap() {
		Map<String,Dummy> test = MapUtil.createMap(objects, (o) -> o.getName());
		
		assertEquals(refMap,test);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCreateMapFailsForDuplicateKeys() {
		MapUtil.createMap(Arrays.asList(dummy1,dummy1), (o) -> o.getName());
	}
	
	@Test
	public void testCreate2DMap() {
		Map<Integer,Map<String,Dummy>> test = MapUtil.create2DMap(objects, (o) -> o.getId(), (o) -> o.getName());
		
		assertEquals(ref2DMap,test);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCreate2DMapFailsForDuplicateKeys() {
		MapUtil.create2DMap(Arrays.asList(dummy1,dummy1), (o) -> o.getId(), (o) -> o.getName());
	}
	
	private class Dummy {
		private String name;
		private Integer id;
		
		public Dummy(String name, Integer id){
			this.name = name;
			this.id = id;
		}
		
		public String getName(){
			return name;
		}
		
		public Integer getId(){
			return id;
		}
	}

}
