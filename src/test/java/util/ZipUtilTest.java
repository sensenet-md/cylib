package util;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.zip.ZipFile;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.util.ResourceExtractor;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;

public class ZipUtilTest {

	private Path zipPath = Paths.get("util","ZipUtilTest","test.zip");
	private Path zipSingleFilePath = Paths.get("util","ZipUtilTest","test.singlefile.zip");
	private Path notZipPath = Paths.get("util","ZipUtilTest","test.txt");

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDecompress() throws Exception {
		Path tempZip = new ResourceExtractor().extractToTempDir(zipPath, "temp.zip");
		Path tempDir = tempZip.getParent();
		ZipUtil.decompress(tempZip, tempDir);
		assertZipContents(tempDir);
	}
	
	@Test
	public void testDecompressSingleFile() throws Exception {
		Path tempZip = new ResourceExtractor().extractToTempDir(zipSingleFilePath, "temp.zip");
		ZipFile zipFile = new ZipFile(tempZip.toFile());
		
		InputStream in = ZipUtil.decompressSingleFile(zipFile);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		
		String line = reader.readLine();
		
		zipFile.close();
		assertEquals("abc",line);
	}
	
	private void assertZipContents(Path extractPath) throws Exception {
		Path a = Paths.get(extractPath.toString(), "a.txt");
		String lineA = Files.readAllLines(a).get(0);
		assertEquals("abc",lineA);
		Path b = Paths.get(extractPath.toString(), "b.txt");
		String lineB = Files.readAllLines(b).get(0);
		assertEquals("def", lineB);
		Path c = Paths.get(extractPath.toString(), "c", "c.txt");
		String lineC = Files.readAllLines(c).get(0);
		assertEquals("ghi", lineC);
	}
	
	@Test
	public void testIsArchive() throws Exception {
		Path tempFile = new ResourceExtractor().extractToTempDir(zipPath, "temp.zip");
		
		Boolean isArchive = ZipUtil.isArchive(tempFile);
		assertEquals(true,isArchive);
		
		tempFile = new ResourceExtractor().extractToTempDir(notZipPath, "temp.txt");
		isArchive = ZipUtil.isArchive(tempFile);
		assertEquals(false,isArchive);
	}
	
	@Test
	public void testCompress() throws Exception {
		Path tempFile = new ResourceExtractor().extractToTempDir(notZipPath, "temp.txt");
		Path zipPath = new TempUtil("zipTest").createTempFile();
		ZipUtil.compressSingleFile(tempFile, zipPath);
		
		InputStream in = ZipUtil.decompressSingleFile(new ZipFile(zipPath.toFile()));
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		
		String line = reader.readLine();
		
		assertEquals("abc",line);
		
		//Path unzipPath = new TempUtil("zipResult",false).createTempDir();
		//ZipUtil.decompress(zipPath, unzipPath);
	}

}
