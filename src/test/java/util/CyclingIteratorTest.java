package util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.util.CyclingIteratorImpl;

public class CyclingIteratorTest {

	private List<Integer> iterables;
	private CyclingIteratorImpl<Integer> testIterator;
	private ArrayList<Integer> threeTimesIterables;

	@Before
	public void setUp() throws Exception {
		this.iterables = Arrays.asList(1,2,3,4);
		this.threeTimesIterables = new ArrayList<Integer>();
		threeTimesIterables.addAll(iterables);
		threeTimesIterables.addAll(iterables);
		threeTimesIterables.addAll(iterables);
		this.testIterator = new CyclingIteratorImpl<Integer>(iterables);
	}

	@Test
	public void testHasNext() {
		for(int i=0;i<100;i++){
			testIterator.next();
			assertTrue(testIterator.hasNext());
		}
	}

	@Test
	public void testNext() {
		List<Integer> testIterate3Times = new ArrayList<Integer>();
		for(int i=0;i < 3*iterables.size();i++){
			testIterate3Times.add(testIterator.next());
		}
		
		assertEquals(threeTimesIterables,testIterate3Times);
		
		
	}

}
