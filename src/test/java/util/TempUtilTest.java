package util;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.util.TempUtil;

public class TempUtilTest {

	private TempUtil util;

	@Before
	public void setUp() throws Exception {
		this.util = new TempUtil("test");
	}

	@Test
	public void testCreateTempFile() throws Exception {
		Path f = util.createTempFile();
		assertTrue(f.toFile().exists());
	}

	@Test
	public void testCreateTempDir() throws Exception {
		Path p = util.createTempDir();
		assertTrue(p.toFile().exists());
	}

}
