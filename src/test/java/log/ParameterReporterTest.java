package log;

import static org.junit.Assert.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cytoscape.cyLib.log.NotReported;
import com.tcb.cytoscape.cyLib.log.ParameterReporter;

public class ParameterReporterTest {

	private AnyClass test;

	@Before
	public void setUp() throws Exception {
		this.test = new AnyClass();		
	}

	@Test
	public void testReportedMembers(){
		assertTrue(test.reportParameters().contains("a"));
	}
	
	@Test
	public void testNotReportedMembers() {
		assertFalse(test.reportParameters().contains("b"));
	}
	
	@Test
	public void testOptionalMember(){
		assertTrue(test.reportParameters().contains("c=2"));
		assertTrue(test.reportParameters().contains("d=null"));
		System.out.println(test.reportParameters());
	}
	
	@Test
	public void testNoClassNameForCollections(){
		assertTrue(test.reportParameters().contains("e={"));
		System.out.println(test.reportParameters());
	}

	
	private class AnyClass implements ParameterReporter {
		private String a = "0";
		
		@NotReported
		private String b = "1";
		
		private Optional<String> c = Optional.of("2");
		private Optional<String> d = Optional.ofNullable(null);
		
		private List<Integer> e = Arrays.asList(0,1);
	}
}

